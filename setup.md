## Stack Used:

- Angular, MongoDB, Nodejs, Express

## Start Node server?

- cd server;
- npm install;
- npm start;
  See swagger documentation: http://localhost:4000/documentation

## Start Angular

- cd FE-angular;
- npm install;
- ng serve
  http://localhost:4200
