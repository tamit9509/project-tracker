const joi = require('joi');
const routeUtil = require('../utils/routeUtils');
const { projectController } = require('../modules/projects/controller');

const routes = [
  {
    path: '/projects',
    method: 'GET',
    joiSchemaForSwagger: {
      group: 'Project',
      description: 'API to fetch Projects',
      model: 'GetProjects',
    },
    handler: projectController.getProjects,
  },
  {
    path: '/projects/:id/start',
    method: 'POST',
    joiSchemaForSwagger: {
      headers: joi
        .object({
          authorization: joi.string().required(),
        })
        .unknown(),
      params: {
        id: routeUtil.validation.mongooseId.required(),
      },
      body: {
        status: joi.boolean(),
        date: joi.date(),
      },
      group: 'Project',
      description: 'API to start Project',
      model: 'StartProject',
    },
    auth: true,
    handler: projectController.startProject,
  },
  {
    path: '/project/workDetails/:id',
    method: 'GET',
    joiSchemaForSwagger: {
      headers: joi
        .object({
          authorization: joi.string().required(),
        })
        .unknown(),
      params: {
        id: routeUtil.validation.mongooseId.required(),
      },
      group: 'Project',
      description: 'API to fetch Projects',
      model: 'GetProjectWorkDetails',
    },
    auth: true,
    handler: projectController.getProjectWorkDetails,
  },
  {
    path: '/projects/ongoing',
    method: 'GET',
    joiSchemaForSwagger: {
      headers: joi
        .object({
          authorization: joi.string().required(),
        })
        .unknown(),
      group: 'Project',
      description: 'API to fetch Projects',
      model: 'GetProjectWorkDetails',
    },
    auth: true,
    handler: projectController.getAllProjects,
  },
];
module.exports = routes;
