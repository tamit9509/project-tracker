const { MONGO_ERROR } = require('../../utils/constants');
const util = require('../../utils/utils');
const { UserModel, SessionModel } = require('../../mongo-models');
const commonFunctions = require('../../utils/commonFunctions');
const errorCodes = require('../../utils/errorCodes');

const controller = {
  userRegister: async (request, response) => {
    request.body.password = commonFunctions.hashPassword(request.body.password);
    request.body.email = request.body.email.toLowerCase();
    const user = new UserModel(request.body);
    try {
      const data = await user.save();
      response.status(200).json({
        success: true,
        message: 'User registered successfully',
        data,
      });
    } catch (err) {
      if (err.code == MONGO_ERROR.DUPLICATE) {
        throw errorCodes.ACCOUNT_ALREADY_EXIST;
      }
    }
  },
  userLogin: async (request, response) => {
    const user = await UserModel.findOne({
      username: request.body.username,
    }).lean();
    const tokenPayload = {
      username: request.body.username,
    };
    const accessToken = commonFunctions.encryptJwt(tokenPayload);
    await UserModel.findOneAndUpdate(
      {
        username: request.body.username,
      },
      { $set: { accessToken, username: request.body.username } },
      { upsert: true }
    );
    response.status(200).json({
      success: true,
      message: 'Login successfull',
      data: {
        accessToken: accessToken,
        username: request.body.username,
      },
    });
  },
  logoutSession: async (request, response) => {
    await SessionModel.deleteOne({ _id: request.user._id });
    response.status(200).json({
      success: true,
      message: 'Logout successfully',
    });
  },
};

module.exports = { authController: controller };
