const moment = require('moment');
const { History, ProjectTrack, Projects } = require('../../mongo-models');

module.exports = class ProjectService {
  constructor() {}

  async getProjects() {
    return await Projects.find({}).lean();
  }

  async updateProject(payload) {
    const userProject = await ProjectTrack.findOne({
      projectId: payload.projectId,
      userId: payload.userId,
    }).lean();
    if (!userProject) {
      return await this.addUserProject(
        payload.projectId,
        payload.userId,
        payload.date,
        payload.status
      );
    } else {
      const promise1 = ProjectTrack.findOneAndUpdate(
        {
          projectId: payload.projectId,
          userId: payload.userId,
        },
        { $set: { status: payload.status } },
        { new: true }
      );
      const promise2 = this.addWorkHistory(
        userProject._id,
        payload.userId,
        payload.status,
        payload.date
      );
      const result = await Promise.all([promise1, promise2]);
      if (payload.status) {
        return result[0];
      } else {
        return result[1];
      }
    }
  }

  async addUserProject(projectId, userId, date, status) {
    const payload = {
      projectId,
      userId,
      startDate: new Date(),
      status,
    };
    return new Promise((resolve, reject) => {
      const obj = new ProjectTrack(payload);
      obj.save().then((res) => {
        resolve(res);
        // this.addWorkHistory(res._id, projectId, userId, true, date);
      });
    });
  }

  async getWorkHistory(projectTrackId, userId, day) {
    const data = await History.findOne({ projectTrackId, userId, day }).lean();
    return data;
  }

  async addWorkHistory(projectTrackId, userId, status, date, sessionName) {
    const day = moment().format('MM/DD/YYYY');
    let productTrackData;
    const todayHistory = await this.getWorkHistory(projectTrackId, userId, day);
    if (todayHistory) {
      let query;
      const $set = {};
      if (!status) {
        const index = todayHistory.history.length - 1;
        const key = `history.${index}.end`;
        const key2 = `history.${index}.sessionName`;
        $set[key] = date;
        $set[key2] = sessionName || todayHistory.history[index].sessionName;
        query = { $set };
        const currentSessionTime = this.calculateSpentTime(
          todayHistory.history[index].start,
          date
        );
        productTrackData = await this.saveNetTimeSpent(
          projectTrackId,
          userId,
          currentSessionTime
        );
      } else {
        const data = {
          start: date,
          sessionName:
            sessionName || `Session ${todayHistory.history.length + 1}`,
        };
        query = { $addToSet: { history: data } };
      }
      await History.updateOne({ projectTrackId, day }, { ...query });
      return productTrackData;
    } else {
      const obj = new History({
        projectTrackId,
        userId,
        day,
        history: [
          {
            start: new Date(),
            sessionName: 'Session 1',
          },
        ],
      });
      await obj.save();
    }
  }
  calculateSpentTime(startDate, endDate) {
    endDate = moment(endDate);
    startDate = moment(startDate);
    const duration = moment.duration(endDate.diff(startDate));
    return {
      hours: duration.hours(),
      minutes: duration.minutes(),
      seconds: duration.seconds(),
    };
  }
  async saveNetTimeSpent(projectTrackId, userId, currentSessionTime) {
    const userProject = await ProjectTrack.findOne({
      _id: projectTrackId,
    }).lean();
    if (userProject) {
      const secondsSum =
        userProject.spendTime.seconds + currentSessionTime.seconds;
      if (secondsSum > 60) {
        currentSessionTime.minutes += 1;
        userProject.spendTime.seconds = secondsSum - 60;
      } else {
        userProject.spendTime.seconds = secondsSum;
      }
      const minutesSum =
        userProject.spendTime.minutes + currentSessionTime.minutes;
      if (minutesSum > 60) {
        currentSessionTime.hours += 1;
        userProject.spendTime.minutes = minutesSum - 60;
      } else {
        userProject.spendTime.minutes = minutesSum;
      }
      userProject.spendTime.hours += currentSessionTime.hours;
      return await ProjectTrack.findOneAndUpdate(
        { _id: userProject._id },
        userProject,
        { new: true }
      );
    }
  }
  async getWorkdetails(projectTrackId) {
    const data = await ProjectTrack.aggregate([
      { $match: { _id: projectTrackId } },
      {
        $lookup: {
          from: 'histories',
          localField: '_id',
          foreignField: 'projectTrackId',
          as: 'workHistory',
        },
      },
    ]);
    return data;
  }
  async getAllProjects(userId) {
    const data = await ProjectTrack.aggregate([
      { $match: { userId } },
      {
        $lookup: {
          from: 'projects',
          localField: 'projectId',
          foreignField: '_id',
          as: 'projectData',
        },
      },
      { $unwind: '$projectData' },
    ]);
    return data;
  }
};
