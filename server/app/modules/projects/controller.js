const ProjectService = require('./service');

const controller = {
  async getProjects(request, response) {
    const obj = new ProjectService();
    try {
      const data = await obj.getProjects();
      response.status(200).json({
        success: true,
        data,
      });
    } catch (err) {
      response.status(400).json({
        success: false,
        data: err.message,
      });
    }
  },
  async startProject(request, response) {
    const projectId = request.params.id;
    const payload = { ...request.body, projectId, userId: request.user._id };
    const obj = new ProjectService();
    try {
      const data = await obj.updateProject(payload);
      response.status(200).json({
        success: true,
        data,
      });
    } catch (err) {
      response.status(500).json({
        success: false,
        message: 'Something went wrong',
        data: err.message,
      });
    }
  },
  async getProjectWorkDetails(request, response) {
    const projectTrackId = request.params.id;
    const obj = new ProjectService();
    try {
      const data = await obj.getWorkdetails(projectTrackId);
      response.status(200).json({
        success: true,
        data,
      });
    } catch (err) {
      console.log(err);
      response.status(500).json({
        success: false,
        message: 'Something went wrong',
      });
    }
  },
  async getAllProjects(request, response) {
    try {
      const obj = new ProjectService();
      const data = await obj.getAllProjects(request.user._id);
      response.status(200).json({
        success: true,
        data,
      });
    } catch (err) {
      console.log(err);
      response.status(500).json({
        success: false,
        message: 'Something went wrong',
      });
    }
  },
};
module.exports = { projectController: controller };
