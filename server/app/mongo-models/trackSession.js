const MONGOOSE = require('mongoose');

const Schema = MONGOOSE.Schema;

const schema = new Schema({
  projectId: { type: Schema.Types.ObjectId, ref: 'projects' },
  userId: { type: Schema.Types.ObjectId, ref: 'users' },
  startDate: { type: Date },
  status: { type: Boolean, default: false },
  expectedEndDate: { type: Date },
  spendTime: {
    hours: { type: Number, default: 0 },
    minutes: { type: Number, default: 0 },
    seconds: { type: Number, default: 0 },
  },
});

schema.set('timestamps', true);
schema.index({ projectId: 1 }, { unique: false });
const ProjectTrack = MONGOOSE.model('projectTrack', schema);
module.exports = { ProjectTrack };
