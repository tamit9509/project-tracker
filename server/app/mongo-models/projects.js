const MONGOOSE = require('mongoose');

const Schema = MONGOOSE.Schema;

const schema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
});

schema.set('timestamps', true);
const Projects = MONGOOSE.model('project', schema);
module.exports = { Projects };
