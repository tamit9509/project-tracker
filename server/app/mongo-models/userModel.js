const MONGOOSE = require('mongoose');
const { USER_ROLE } = require('../utils/constants');
const schema = MONGOOSE.Schema;

const user = new schema({
  username: { type: String, require: true },
  accessToken: { type: String },
});

user.set('timestamps', true);
const UserModel = MONGOOSE.model('user', user);
module.exports = { UserModel };
