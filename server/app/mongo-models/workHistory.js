const MONGOOSE = require('mongoose');

const Schema = MONGOOSE.Schema;

const schema = new Schema({
  projectTrackId: { type: Schema.Types.ObjectId, ref: 'projectTracks' },
  userId: { type: Schema.Types.ObjectId, ref: 'users' },
  day: { type: Date },
  history: [
    {
      sessionName: { type: String },
      start: { type: Date },
      end: { type: Date },
    },
  ],
});

schema.set('timestamps', true);
schema.index({ projectId: 1 }, { unique: false });
schema.index({ userId: 1 }, { unique: false });
const History = MONGOOSE.model('history', schema);
module.exports = { History };
