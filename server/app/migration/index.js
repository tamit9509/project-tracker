const migrate = require('./projects');

module.exports = async function startMigration() {
  await migrate.addProjects();
};
