const { Projects } = require('../mongo-models/projects');
const { DUMMY_PROJECTS } = require('../utils/constants');
const migrate = {
  async addProjects() {
    const projects = await Projects.find({}).lean();
    if (!projects || !projects.length) {
      await Projects.insertMany(DUMMY_PROJECTS);
    }
  },
};
module.exports = migrate;
