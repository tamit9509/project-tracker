const CONSTANTS = {
  SECURITY: {
    JWT_SIGN_KEY: 'fasdkfjklandfkdsfjladsfodfafjalfadsfkads',
    BCRYPT_SALT: 8,
  },
  EXTERNAL_APIS: {
    WEATHER_UPDATE: '/data/2.5/onecall',
    NEWS: '/v2/top-headlines',
  },
  WEATHER_PARAMS: {
    ONE_CALL_EXCLUDE: 'current,minutely,hourly',
    UNITS: {
      STANDARD: 'standard',
      METRIC: 'metric',
      IMPERIAL: 'imperial',
    },
  },
  NEWS_PARAMS: {
    CATEGORY: {
      TECHNOLOGY: 'technology',
      SPORTS: 'sports',
      SCIENCE: 'science',
      HEALTH: 'health',
      GENERAL: 'general',
      ENTERTAINMENT: 'entertainment',
      BUSINESS: 'business',
    },
    PAGE_SIZE: 20,
  },
  DUMMY_PROJECTS: [
    {
      name: 'CRM Management',
      description:
        'Customer relationship management is a process in which a business or other organization administers its interactions with customers, typically using data analysis to study large amounts of information.',
    },
    {
      name: 'E-commerce',
      description:
        'E-commerce is the activity of electronically buying or selling of products on online services or over the Internet',
    },
    {
      name: 'E-learning App',
      description:
        'E-learning, also referred to as online learning or electronic learning, is the acquisition of knowledge which takes place through electronic technologies',
    },
  ],
};
module.exports = CONSTANTS;
