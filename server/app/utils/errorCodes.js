const errorCodes = {
  BAD_REQUEST: {
    statusCode: 400,
    message: 'Bad Request',
    type: 'BAD_REQUEST',
  },
  SOMETHING_WENT_WRONG: {
    statusCode: 500,
    message: 'Something went wrong.',
    type: 'WENT_WRONG',
  },
  DUPLICATE_ENTRY: {
    statusCode: 400,
    message: '{{key}} Already Exists',
    type: 'DUPLICATE_ENTRY',
  },
  ACCOUNT_ALREADY_EXIST: {
    statusCode: 400,
    message: 'Account Already Exists',
    type: 'USER_NAME_ALREADY_EXIST',
  },
  INVALID_CREDENTIALS: {
    statusCode: 400,
    message: 'Invalid email or password',
    type: 'INVALID_CREDENTIALS',
  },
};

module.exports = errorCodes;
