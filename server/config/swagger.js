module.exports = {
  "swagger": "2.0",

  "title": "Music API Documentation",
  "info": {
    version: '2.0.0',
    title: 'APIs Docs',
    description: 'APIs.',
    "termsOfService": "http://swagger.io/terms/",
    "contact": {
      "name": "Eduseeker team"
    },
    "license": {
      "name": "MIT"
    }
  },
  "paths": {},
  "definitions": {},
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ]
};