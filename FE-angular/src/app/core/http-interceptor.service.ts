import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { AlertService } from './alert.service';
import { USER_DETAILS } from './constants';


@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {
    private requests: HttpRequest<any>[] = [];
    constructor(
        public router: Router,
        public authService: AuthService,
        private alertService: AlertService
    ) { }

    /** Request interceptor **/
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        debugger
        if (this.authService.isLogIn) {
            const token = this.authService.getUserData(USER_DETAILS.Token);
            req = req.clone({ headers: req.headers.set('authorization', token) });
        }
        this.requests.push(req);

        return next.handle(req).pipe(
            tap((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    this.removeRequest(req);
                }
            }, (err: any) => {
                this.removeRequest(req);
                if (err instanceof HttpErrorResponse) {
                    if (err.status === 0) {
                        this.alertService.error('Network error!');
                    }
                    else if (err.status === 401) {
                        this.alertService.error('Your session has been expired. Please login again');
                    }
                    else if(err.status==403){
                    }
                }
            })
        );
    }
    /** Remove request **/
    removeRequest(req: HttpRequest<any>) {
        // this.loaderService.hide();
        const i = this.requests.indexOf(req);
        this.requests.splice(i, 1);
    }
}