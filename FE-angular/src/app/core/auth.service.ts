import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { USER_DETAILS } from './constants';


@Injectable({
    providedIn: 'root'
})
export class AuthService {
    name: string;
    UserDetails = { ...USER_DETAILS };
    isLogIn: boolean;

    constructor(
    ) {
        this.name=this.getUserData(USER_DETAILS.Name)?this.getUserData(USER_DETAILS.Name):'';
        this.isLogIn=!!this.name;
    }

    /** Save User data locally after login */
    saveUserData(userData) {
        this.name = userData[this.UserDetails.Name] || '';
        localStorage.setItem(this.UserDetails.Token, userData[this.UserDetails.Token]);
        localStorage.setItem(this.UserDetails.Name, userData[this.UserDetails.Name]);
        this.isLogIn=true;
    }

    getUserData(key: string) {
        return localStorage.getItem(key);
    }
}
