export const APIS={
    projects:'/projects',
    login:'/api/login',
    ongoing:'/projects/ongoing',
    history:'/project/workDetails'
}
export const USER_DETAILS = {
    Token: 'accessToken',
    Name: 'username',
  }