import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from '../core/alert.service';
import { AuthService } from '../core/auth.service';
import { APIS } from '../core/constants';
import { HttpService } from '../core/http-service.service';
import * as moment from 'moment';
@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {
  projects;
  activeProduct;
  timerId;
  userErolledProjects;
  trackHistory;
  constructor(
    private httpService:HttpService,
    private alertService: AlertService,
    public authService: AuthService,
    private modalService: NgbModal,
    private activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
    
    this.httpService.getData(APIS.projects).subscribe(res=>{
      this.projects=res.data;
    });
    this.getMyProjects();
  }
  login(){
    this.alertService.inputAlert('Enter User name', 'Submit').then(res=>{
      if(res.value){
        this.httpService.postData(APIS.login,{username:res.value}).subscribe((res:any)=>{
          this.authService.saveUserData(res.data);
        })
      }
    })
  }
  enroll(project){
    if(!this.authService.isLogIn){
      this.login();
    }else {
      this.updateProject(project, project._id).then(res=>{
        this.getMyProjects();
      });
    }
  }
  updateProject(product, projectId, status?:boolean){
    if(status && this.activeProduct && this.activeProduct.status){
      alert('Please stop existing tracker')
      return;
    }
    return new Promise((resolve, reject)=>{
      this.httpService.postData(`${APIS.projects}/${projectId}/start`, {status, date: new Date()}).subscribe((res:any)=>{
        product.status=res.data.status;
        product.spendTime=res.data.spendTime;
        this.activeProduct=product;
        if(!status){
          this.pauseTracker();
        }else{
          if(this.timerId){
            this.pauseTracker();
          }
          this.startTimer(product);
        }
        resolve(true);
      })

    })

  }
  startTimer(product) {
    this.timerId=setInterval(() => {
      product.spendTime.seconds++;
      if (product.spendTime.minutes > 59) {
        product.spendTime.hours++;
        product.spendTime.minutes = 0;
      }
      if (product.spendTime.seconds >59) {
        product.spendTime.minutes++ ;
        product.spendTime.seconds = 0;
      }
    }, 1000)
  }
  pauseTracker(){
    clearInterval(this.timerId);
  }
  getMyProjects(){
    this.httpService.getData(APIS.ongoing).subscribe(res=>{
      debugger
      this.userErolledProjects=res.data;
      let trackerActiveProduct=this.userErolledProjects.find(prod=>prod.status)
      if(trackerActiveProduct){
        this.startTimer(trackerActiveProduct);
      }
    })
  }
  getSpentTime(project){
    return `${project.spendTime.hours}h : ${project.spendTime.minutes}m : ${project.spendTime.seconds}s`
  }
  getHistory(project, component){
    this.modalService.open(component, { centered: true, size: 'lg' });
    this.httpService.getData(`${APIS.history}/${project._id}`).subscribe(res=>{
      this.trackHistory=res.data[0];
    })
  }
  closeModal() {
    this.activeModal.close();
  }
  getTime(date){
    return moment(date).format('hh:mm:ss')
  }
}
